import React, { Component } from 'react';
import { Container, Grid, Header, Icon } from 'semantic-ui-react';
import NavigationContainer from '../container/navigationContainer';
import MenuContainer from '../container/menuContainer';

class NotFoundPage extends Component {
    render() {
        return (
            <div>
                <NavigationContainer />
                <Container className="page">
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column width={2}>
                                <MenuContainer />
                            </Grid.Column>
                            <Grid.Column width={14}>
                                <Header as='h2' icon textAlign='center'>
                                    <Icon name='window close outline' color='red' />
                                    Oops!
                                    <Header.Subheader>
                                        We can't seem to find the page you're looking for.
                                    </Header.Subheader>
                                </Header>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </div>
        );
    }
}

export default NotFoundPage;