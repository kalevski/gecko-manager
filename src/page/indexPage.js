import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import AuthService from '../service/authService';

class IndexPage extends Component {

    authService = AuthService.getInstance();

    render() {
        return !this.authService.isAuthenticated() ? (
            <Redirect to="/login"/>
        ) : (
            <Redirect to="/dashboard"/>
        )
    }
}

export default IndexPage;