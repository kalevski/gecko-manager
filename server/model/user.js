class UserDto {
    id = null;
    username = null;
    mail = null;
    password = null;
    role = null;
    lastLogin = null;
    info = {
        fullname: null,
        age: null,
        ocupation: null,
        company: null
    };
    settings = {
        infoWizzard: false
    };
}

export default UserDto;