import Enum from 'enum';

export default new Enum([
    'VISITOR',
    'MEETUP_VISITOR',
    'INTERN',
    'EMPLOYEE',
    'MANAGER',
    'ADMIN'
]);