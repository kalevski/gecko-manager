import UserService from '../service/userService';
import ResponseDto from '../dto/responseDto';
import Logger from '../helper/logger';

export default (userRoleList = null) => {    
    return (request, response, next) => {
        let token = request.get('Authorization');
        let userService = new UserService();
        let responseDto = new ResponseDto();
        let logger = Logger.getInstance();
        userService.canAccess(token, userRoleList).then((can) => {
            if (can) {
                next();
            } else {
                responseDto.status = 401;
                responseDto.code = 'unauthorized';
                responseDto.message = 'unauthorized' // TODO: implement error messages
                response.status(200).json(responseDto);
            }
        }).catch((error) => {
            logger.error('[AUTH MIDDLEWARE]', error);
            responseDto.status = 500;
            responseDto.code = 'internal_server_error';
            responseDto.message = error.message // TODO: implement error messages
            response.status(200).json(responseDto);
        });
    }
};