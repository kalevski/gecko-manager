import Controller from '../core/controller';
import Logger from '../helper/logger';
import UserService from '../service/userService';

class SignupController extends Controller {

    post(request, response) {
        let username = request.body.username;
        let mail = request.body.mail;
        let password = request.body.password;
        let fullname = request.body.fullname;

        let userService = new UserService();
        let logger = Logger.getInstance();

        userService.signup(username, mail, password, fullname).then((responseDto) => {
            response.status(200).json(responseDto);
        }).catch((responseDto) => {
            logger.error('[POST:/signup]', responseDto);
            response.status(200).json(responseDto);
        });
    }
    

}

export default SignupController;