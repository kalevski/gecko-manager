import Redis from '../core/adapter/redis';

class UserTokenDao {

    redis = Redis.getInstance();
    ttl = 60 * 30; // 30 after last request

    get(token) {
        return this.redis.client.get('user-token:' + token).then((value) => {
            if(value !== null) {
                this.redis.client.expire('user-token:' + token, this.ttl);
            }
            return JSON.parse(value);
        });
    }

    create(userDto) {
        return this.redis.client.set('user-token:' + userDto.token, JSON.stringify(userDto)).then(() => {
            this.redis.client.expire('user-token:' + userDto.token, this.ttl);
            return userDto;
        });
    }
}

export default UserTokenDao;