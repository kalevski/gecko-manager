import Redis from '../core/adapter/redis';

class UserDao {

    redis = Redis.getInstance();

    get(id) {
        return this.redis.client.get('user:' + id).then((value) => {
            return JSON.parse(value);
        });
    }

    create(user) {
        return this.redis.client.get('user:' + user.id).then((value) => {
            if (value !== null) throw new Error('user_exist');
            return this.redis.client.get('user:username:' + user.username);
        }).then((value) => {
            if (value !== null) throw new Error('username_exist');
            return this.redis.client.get('user:mail:' + user.mail);
        }).then((value) => {
            if (value !== null) throw new Error('mail_exist');
            return this.redis.client.set('user:' + user.id, JSON.stringify(user));
        }).then((value) => {
            this.redis.client.set('user:username:' + user.username, user.id);
            this.redis.client.set('user:mail:' + user.mail, user.id);
            return user;
        });
    }

    update(user) {
        let oldUser = null;
        return this.delete(user.id).then((value) => {
            return this.create('user:' + user.id, JSON.stringify(user));
        });
    }

    delete(id) {
        return this.redis.client.del('user:' + id).then(() => {
            return this.redis.client.del('user:username:' + id);
        }).then(() => {
            return this.redis.client.del('user:mail:' + id);
        });
    }

    getByUsername(username) {
        return this.redis.client.get('user:username:' + username).then((value) => {
            if (value === null) return null;
            else return this.get(value);
        });
    }

    getByMail(mail) {
        return this.redis.client.get('user:mail:' + username).then((value) => {
            if (value === null) return null;
            else return this.get(value);
        });
    }
}

export default UserDao;