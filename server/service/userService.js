import uuid from 'uuid/v4';
import md5 from 'md5';
import Logger from '../helper/logger';
import ResponseDto from '../dto/responseDto';
import UserDto from '../dto/userDto';
import User from '../model/user';
import UserDao from '../dao/userDao';
import UserTokenDao from '../dao/userTokenDao';
import UserRoleEnum from '../enum/userRoleEnum';
class UserService {

    userDao = new UserDao();
    userTokenDao = new UserTokenDao();

    defaultRoleMask = [UserRoleEnum.VISITOR];

    signup(username, mail, password, fullname) {
        return new Promise((resolve, reject) => {
            let responseDto = new ResponseDto();
            if (typeof username === 'undefined' || typeof mail === 'undefined'
                || typeof password === 'undefined' || typeof fullname === 'undefined') {
                responseDto.status = 412;
                responseDto.code = 'missing_parameter';
                responseDto.message = 'missing_parameter'; // TODO: create error code table
                resolve(responseDto);
            } else {
                this._createUser(username, mail, password, fullname).then((user) => {
                    let token = this._createToken();
                    let userDto = this._createUserDto(user, token);
                    this.userTokenDao.create(userDto);
                    return userDto;
                }).then((userDto) => {
                    responseDto.status = 201;
                    responseDto.data = userDto;
                    resolve(responseDto);
                }).catch((error) => {
                    responseDto.status = 500;
                    responseDto.code = error.message;
                    responseDto.message = error.message; // TODO: create error code table
                    reject(responseDto);
                })
            }
        });
    }

    login(username, password) {
        return new Promise((resolve, reject) => {
            let responseDto = new ResponseDto();
            if (typeof username === 'undefined' || password === 'undefined') {
                responseDto.status = 412;
                responseDto.code = 'missing_parameter';
                responseDto.message = 'missing_parameter'; // TODO: create error code table
                resolve(responseDto);
            } else {
                this.userDao.getByUsername(username).then((user) => {
                    if(user === null) {
                        responseDto.status = 451;
                        responseDto.code = 'wrong_credentials';
                        responseDto.message = 'wrong_credentials'; // TODO: create error code table
                        resolve(responseDto);
                    } else if (md5(password) === user.password) {
                        let token = this._createToken(user);
                        let userDto = this._createUserDto(user, token);
                        this.userTokenDao.create(userDto);
                        return userDto;
                    } else {
                        responseDto.status = 451;
                        responseDto.code = 'wrong_credentials';
                        responseDto.message = 'wrong_credentials'; // TODO: create error code table
                        resolve(responseDto);
                    }
                }).then((userDto) => {
                    responseDto.status = 200;
                    responseDto.data = userDto;
                    resolve(responseDto);
                }).catch((error) => {
                    responseDto.status = 500;
                    responseDto.code = error.message;
                    responseDto.message = error.message; // TODO: create error code table
                    resolve(responseDto);
                })
            }
        });
    }

    canAccess(token, userRoleList = null) {
        return this.userTokenDao.get(token).then((userDto) => {
            if (userDto === null) {
                return false;
            }
            if (userRoleList === null) {
                return true;
            } else {
                let roleMask = UserRoleEnum.get(userDto.role);
                for (let ROLE of userRoleList) {
                    if (roleMask.has(ROLE)) {
                        return true;
                    }
                }
                return false;
            }
        });
    }

    getUser(username) {
        return new Promise((resolve, reject) => {
            let responseDto = new ResponseDto();
            if (typeof username === 'undefined') {
                responseDto.status = 412;
                responseDto.code = 'missing_parameter';
                reject(responseDto);
            } else {
                this.userDao.getByUsername(username).then((user) => {
                    if (user !== null) {
                        let token = null;
                        let userDto = this._createUserDto(user, token);
                        responseDto.status = 200;
                        responseDto.data = userDto;
                        resolve(responseDto);
                    } else {
                        console.log();
                        responseDto.status = 404;
                        responseDto.code = 'user_not_found';
                        responseDto.message = 'user_not_found'; // TODO: create error code table
                        resolve(responseDto);
                    }
                }).catch((error) => {
                    responseDto.status = 500;
                    responseDto.code = 'internal_server_error';
                    responseDto.message = error.message;
                    reject(responseDto);
                })
            }
        });
    }

    _createToken() {
        return uuid();
    }

    _createUserDto(user, token) {
        let userDto = new UserDto();
        userDto.username = user.username;
        userDto.mail = user.mail;
        userDto.role = user.role;
        userDto.lastLogin = user.lastLogin;
        userDto.info = user.info;
        userDto.settings = user.settings;
        userDto.token = token;
        return userDto;
    }

    _createUser(username, mail, password, fullname) {
        let user = new User();
        user.id = uuid();
        user.username = username;
        user.password = md5(password);
        user.mail = mail;
        user.info.fullname = fullname;
        user.role = this.defaultRoleMask.join(' | ');
        return this.userDao.create(user);
    }
}

export default UserService;