'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mysql = require('mysql');

var _mysql2 = _interopRequireDefault(_mysql);

var _logger = require('../helper/logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MySQL = function () {
    function MySQL() {
        var _this = this;

        _classCallCheck(this, MySQL);

        this.logger = _logger2.default.getInstance();
        this.pool = null;

        this.pool = _mysql2.default.createPool({
            connectionLimit: 5,
            host: process.env['DB_HOST'],
            user: process.env['DB_USER'],
            password: process.env['DB_PASSWORD'],
            database: process.env['DB_NAME']
        });
        this.query('SELECT 1 + 1;').then(function () {
            _this.logger.debug('Database pool is ready!');
        }).catch(function (error) {
            _this.logger.error('Something went wrong with database connection!', error);
        });
    }

    _createClass(MySQL, [{
        key: 'query',
        value: function query(_query) {
            var _this2 = this;

            var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

            return new Promise(function (resolve, reject) {
                _this2.pool.query(_query, params, function (error, results, fields) {
                    if (error) {
                        this.logger.error('DATABASE: ', error);
                    } else {
                        resolve({
                            results: results,
                            fields: fields
                        });
                    }
                });
            });
        }
    }]);

    return MySQL;
}();

var instance = null;
MySQL.getInstance = function () {
    if (instance === null) {
        instance = new MySQL();
    }
    return instance;
};

exports.default = MySQL;
//# sourceMappingURL=mysql.js.map