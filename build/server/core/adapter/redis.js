'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _thenRedis = require('then-redis');

var _logger = require('../../helper/logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Redis = function Redis() {
    var _this = this;

    _classCallCheck(this, Redis);

    this.client = null;
    this.logger = _logger2.default.getInstance();

    this._onError = function (err) {
        _this.logger.error('Caching System: ' + err);
    };

    this.client = (0, _thenRedis.createClient)(process.env.REDIS_URL);
    this.client.on('error', this._onError);
};

var instance = null;
Redis.getInstance = function () {
    if (instance === null) {
        instance = new Redis();
    }
    return instance;
};

exports.default = Redis;
//# sourceMappingURL=redis.js.map