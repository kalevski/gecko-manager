'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressWs = require('express-ws');

var _expressWs2 = _interopRequireDefault(_expressWs);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _dotenv = require('dotenv');

var _dotenv2 = _interopRequireDefault(_dotenv);

var _config = require('../config/config.dev');

var _config2 = _interopRequireDefault(_config);

var _config3 = require('../config/config.prod');

var _config4 = _interopRequireDefault(_config3);

var _logger = require('../helper/logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ExpressServer = function () {
    function ExpressServer() {
        _classCallCheck(this, ExpressServer);

        this.logger = _logger2.default.getInstance();
        this.express = (0, _expressWs2.default)((0, _express2.default)());
        this.controllerMap = {};

        this.express.app.use((0, _cors2.default)());
        this.express.app.use(_bodyParser2.default.json());
        this.express.app.disable('x-powered-by');

        var ENV = null;
        var DB_CONFIG = null;
        var fileData = null;

        if (process.env['ENV'] === 'DEV') {
            ENV = _config2.default;
            try {
                fileData = _fs2.default.readFileSync(_path2.default.join(__dirname, '../../.env.database.local'));
                DB_CONFIG = _dotenv2.default.parse(fileData);
            } catch (error) {
                try {
                    fileData = _fs2.default.readFileSync(_path2.default.join(__dirname, '../../.env.database'));
                    DB_CONFIG = _dotenv2.default.parse(fileData);
                } catch (error) {
                    this.logger.error('create .env.database configuration');
                }
            }
        } else {
            ENV = _config4.default;
            try {
                fileData = _fs2.default.readFileSync(_path2.default.join(__dirname, '../../../.env.database'));
                DB_CONFIG = _dotenv2.default.parse(fileData);
            } catch (error) {
                this.logger.error('create .env.database configuration');
            }
        }

        process.env['ROOT'] = _path2.default.join(__dirname, '../');
        process.env['PORT'] = process.env['PORT'] || ENV['PORT'];
        process.env['WEBAPP_INDEX'] = ENV['WEBAPP_INDEX'];
        process.env['WEBAPP_STATIC'] = ENV['WEBAPP_STATIC'];

        process.env['DB_HOST'] = DB_CONFIG['DB_HOST'];
        process.env['DB_USER'] = DB_CONFIG['DB_USER'];
        process.env['DB_PASSWORD'] = DB_CONFIG['DB_PASSWORD'];
        process.env['DB_NAME'] = DB_CONFIG['DB_NAME'];

        // additional 
        process.env['SENDGRID_API_KEY'] = process.env['SENDGRID_API_KEY'] || ENV['SENDGRID_API_KEY'];
    }

    _createClass(ExpressServer, [{
        key: 'addMiddleware',
        value: function addMiddleware(route, middleware) {
            this.express.app.use(route, middleware);
        }
    }, {
        key: 'addController',
        value: function addController(route, controller) {
            this.controllerMap[route] = new controller(this.express, route);
        }
    }, {
        key: 'addStatic',
        value: function addStatic(route, dir) {
            var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

            this.express.app.use(route, _express2.default.static(_path2.default.join(__dirname, _path2.default.join('../', dir)), options));
        }
    }, {
        key: 'run',
        value: function run() {
            this.express.app.listen(process.env['PORT']);
            this.logger.info('Server started on port ' + process.env['PORT']);
        }
    }]);

    return ExpressServer;
}();

exports.default = ExpressServer;
//# sourceMappingURL=expressServer.js.map