'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ResponseDto = function ResponseDto() {
    _classCallCheck(this, ResponseDto);

    this.status = 200;
    this.code = '';
    this.message = '';
    this.data = null;
};

exports.default = ResponseDto;
//# sourceMappingURL=responseDto.js.map