"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UserDto = function UserDto() {
    _classCallCheck(this, UserDto);

    this.username = null;
    this.mail = null;
    this.role = null;
    this.lastLogin = null;
    this.token = null;
    this.info = {
        fullname: null,
        age: null,
        ocupation: null,
        company: null
    };
    this.settings = {
        infoWizzard: false
    };
};

exports.default = UserDto;
//# sourceMappingURL=userDto.js.map