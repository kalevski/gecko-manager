'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _redis = require('../core/adapter/redis');

var _redis2 = _interopRequireDefault(_redis);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UserTokenDao = function () {
    function UserTokenDao() {
        _classCallCheck(this, UserTokenDao);

        this.redis = _redis2.default.getInstance();
        this.ttl = 60 * 30;
    }

    _createClass(UserTokenDao, [{
        key: 'get',
        // 30 after last request

        value: function get(token) {
            var _this = this;

            return this.redis.client.get('user-token:' + token).then(function (value) {
                if (value !== null) {
                    _this.redis.client.expire('user-token:' + token, _this.ttl);
                }
                return JSON.parse(value);
            });
        }
    }, {
        key: 'create',
        value: function create(userDto) {
            var _this2 = this;

            return this.redis.client.set('user-token:' + userDto.token, JSON.stringify(userDto)).then(function () {
                _this2.redis.client.expire('user-token:' + userDto.token, _this2.ttl);
                return userDto;
            });
        }
    }]);

    return UserTokenDao;
}();

exports.default = UserTokenDao;
//# sourceMappingURL=userTokenDao.js.map