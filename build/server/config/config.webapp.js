'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = [{
    path: '/',
    code: 200
}, {
    path: '/login',
    code: 200
}, {
    path: '/signup',
    code: 200
}, {
    path: '/dashboard',
    code: 200
}];
//# sourceMappingURL=config.webapp.js.map