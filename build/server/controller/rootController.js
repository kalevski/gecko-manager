'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _urlPattern = require('url-pattern');

var _urlPattern2 = _interopRequireDefault(_urlPattern);

var _controller = require('../core/controller');

var _controller2 = _interopRequireDefault(_controller);

var _config = require('../config/config.webapp');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RootController = function (_Controller) {
    _inherits(RootController, _Controller);

    function RootController() {
        _classCallCheck(this, RootController);

        return _possibleConstructorReturn(this, (RootController.__proto__ || Object.getPrototypeOf(RootController)).apply(this, arguments));
    }

    _createClass(RootController, [{
        key: 'get',
        value: function get(request, response) {
            var status = 404;
            var indexPath = _path2.default.join(process.env.ROOT, process.env.WEBAPP_INDEX);
            var pattern = new _urlPattern2.default(request.path);
            for (var i = 0; i < _config2.default.length; i++) {
                if (pattern.match(_config2.default[i].path) !== null) {
                    status = _config2.default[i].code;
                    break;
                }
            }
            response.status(status).sendFile(indexPath);
        }
    }]);

    return RootController;
}(_controller2.default);

exports.default = RootController;
//# sourceMappingURL=rootController.js.map